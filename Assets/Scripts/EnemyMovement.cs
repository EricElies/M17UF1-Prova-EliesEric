﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    
    Vector3 End_pos;
    Vector3 Start_pos;
    public GameObject enemy;
   
    
    // Start is called before the first frame update
    void Start()
    {
        
        Start_pos = enemy.transform.position;
        End_pos = Start_pos + new Vector3(0, -50, 0); 
    }

    // Update is called once per frame
    void Update()
    {

        moveAlien();
      
    }


    void moveAlien() {     
        if(CompareTag("Alien"))
            enemy.transform.position = new Vector3(enemy.transform.position.x,enemy.transform.position.y -0.001f);

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            Destroy(collision.gameObject);
        }
    }



    private void OnCollision2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Bullet")
        {
            Destroy(collision.gameObject);
        }
    }

}
