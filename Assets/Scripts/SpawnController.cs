﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    public GameObject enemy;

    void Start()
    {
        InvokeRepeating("Spawner", 1, 5);
    }

    // Update is called once per frame
    void Spawner()
    {
        GameObject e = Instantiate(enemy, new Vector3(Random.Range(-5f, 5f), Random.Range(7f, 8f)), Quaternion.identity);

    }
}
