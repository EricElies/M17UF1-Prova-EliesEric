﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{
    public float speed;
    private float moveInputH;
    private float moveInputV;

    private Rigidbody2D rb;
    public Camera cam;

    public GameObject player;
    public Vector3 initPos;


    public static object Instance { get; internal set; }

    private void Start()
    {
        cam = GetComponent<Camera>();
        rb = GetComponent<Rigidbody2D>();
        initPos = transform.position;

    }

    private void FixedUpdate()
    {

        moveInputH = Input.GetAxis("Horizontal");
        moveInputV = Input.GetAxis("Vertical");
        rb.velocity = new Vector3(moveInputH * speed, moveInputV * speed, rb.velocity.y);

    }
    

    public void ResetPosition()
    {
       transform.position = initPos;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
        Destroy(collision.gameObject);
        ResetPosition();
    }

}